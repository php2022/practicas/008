<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejercicio5Destino.php" method="get">
            <div>
                <label for="nombre">
                    Nombre:
                </label>
                <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre">
            </div>
            <br>
            <div>
                <label for="apellidos">
                    Apellidos:
                </label>
                <input type="text" name="apellidos" id="apellidos" placeholder="Introduce tus apellidos">
            </div>
            <br>
            <div>
                <label for="direccion">
                    Dirección:
                </label>
                <input type="text" name="direccion" id="direccion" placeholder="Introduce tu dirección">
            </div>
            <br>
            <div>
                <label for="cp">
                    Código postal:
                </label>
                <input type="number" name="cp" id="cp" placeholder="Introduce tu código postal">
            </div>
            <br>
            <div>
                <label for="telefono">
                    Teléfono:
                </label>
                <input type="number" name="telefono" id="telefono" placeholder="Introduce tu teléfono">
            </div>
            <br>
            <div>
                <label for="email">
                    Correo:
                </label>
                <input type="email" name="email" id="email" placeholder="Introduce tu email">
            </div>
            <br>
            <div>
                <button>ENVIAR</button>
            </div>
        </form>
    </body>
</html>
