<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejercicio3Destino.php" method="get">
            <div>
                <label for="nombre">
                    Nombre:
                </label>
                <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre">
            </div>
            <br>
            <div>
                <label for="email">
                    Email:
                </label>
                <input type="email" name="email" id="email" placeholder="Introduce tu email">
            </div>
            <br>
            <div>
                <label for="telefono">
                    Teléfono:
                </label>
                <input type="text" name="telefono" id="telefono" placeholder="Introduce tu teléfono">
            </div>
            <br>
            <div>
                <button>ENVIAR</button>
            </div>
        </form>
    </body>
</html>
